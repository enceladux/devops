# Use the official PHP 7.4 FPM (FastCGI Process Manager) image as the base image
FROM php:7.4-fpm

# Install required packages and PHP extensions
RUN apt-get update && apt-get install -y \
       libzip-dev \
       zip \
       unzip \
   && docker-php-ext-configure zip \
   && docker-php-ext-install zip

# Install Composer globally in the image
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set the working directory for the application
WORKDIR /var/www/html

# Copy the application files into the working directory
COPY . .

# Update the dependencies and install the Laravel project's dependencies
RUN composer update
RUN composer install

# Set the command to start the application server
CMD php artisan serve --host=0.0.0.0 --port=8000

# Expose port 8000 so that it can be accessed from outside the container
EXPOSE 8000

